package ictgradschool.industry.designpatternsii.ex02.model;


import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
    private Course _adaptee;
    public CourseAdapter(Course courseModel) {
        _adaptee = courseModel;
        _adaptee.addCourseListener(this);
    }


    @Override
    public void courseHasChanged(Course course) {
        fireTableDataChanged();
    }

    @Override
    public int getRowCount() {
        return _adaptee.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    final int STUDENT_ID = 0;
    final int STUDENT_LNAME = 1;
    final int STUDENT_FNAME = 2;
    final int STUDENT_EXAM = 3;
    final int STUDENT_TEST = 4;
    final int STUDENT_ASSIGNMENT = 5;
    final int STUDENT_OVERALL = 6;


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        StudentResult sr = _adaptee.getResultAt(rowIndex);

        switch (columnIndex) {
            case STUDENT_ID:
                return sr._studentID;
            case STUDENT_LNAME:
                return sr._studentSurname;
            case STUDENT_FNAME:
                return sr._studentForename;
            case STUDENT_EXAM:
                return sr.getAssessmentElement(StudentResult.AssessmentElement.Exam);
            case STUDENT_TEST:
                return sr.getAssessmentElement(StudentResult.AssessmentElement.Test);
            case STUDENT_ASSIGNMENT:
                return sr.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
            case STUDENT_OVERALL:
                return sr.getAssessmentElement(StudentResult.AssessmentElement.Overall);
            default:
                return "Flibble";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case STUDENT_ID:
                return "Student ID";
            case STUDENT_LNAME:
                return "Surname";
            case STUDENT_FNAME:
                return "Forename";
            case STUDENT_EXAM:
                return "Exam";
            case STUDENT_TEST:
                return "Test";
            case STUDENT_ASSIGNMENT:
                return "Assignment";
            case STUDENT_OVERALL:
                return "Overall";
            default:
                return super.getColumnName(column);
        }

    }
/**********************************************************************
     * YOUR CODE HERE
     */
}