package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

import javax.swing.*;
import java.awt.event.ActionEvent;


public class StatisticsPanelAdapter implements CourseListener {

    StatisticsPanel panel;
    Course adapterCourse;

    public StatisticsPanelAdapter(StatisticsPanel panel) {
        this.panel = panel;
    }

    @Override
    public void courseHasChanged(Course course) {
        panel.repaint();
    }

    public void setModel (Course course) {
        this.adapterCourse = course;
        adapterCourse.addCourseListener(this);
    }

}
