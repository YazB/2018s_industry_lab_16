package ictgradschool.industry.designpatternsii.ex02.gui;

import ictgradschool.industry.designpatternsii.ex02.model.Course;
import ictgradschool.industry.designpatternsii.ex02.model.CourseListener;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class DistributionPanelAdapter implements CourseListener {
    //Constructor for new distribution panel adapter and pass in dp created in teh main
    DistributionPanel panel;
    Course course;

    public DistributionPanelAdapter(DistributionPanel panel) {
        this.panel = panel;
    }

    @Override
    public void courseHasChanged(Course course) {
        panel.repaint();
    }

    public void setModel (Course course) {
        this.course = course;
        course.addCourseListener(this);
    }
}
